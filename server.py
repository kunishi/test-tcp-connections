#!/usr/bin/env python

import sys
import socket
from contextlib import closing

def main():
    host = '127.0.0.1'
    port = 12345
    backlog = 10
    bufsize = 4096

    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(None)
        with closing(sock):
            sock.bind((host, port))
            sock.listen(backlog)
            while True:
                conn, address = sock.accept()
                with conn:
                    print('Connected from ', address)
                    while True:
                        msg = conn.recv(bufsize)
                        if not msg: break
                        print(msg.rstrip())
                        conn.send("0\n".encode('utf-8'))
        return
    except KeyboardInterrupt as msg:
        print(msg)
        if conn != None:
            conn.close()
            conn = None
        if sock != None:
            sock.close()
            sock = None
        sys.exit()
    except OSError as msg:
        print(msg)
        print(sock)
        if conn != None:
            conn.close()
            conn = None
        if sock != None:
            sock.close()
            sock = None
        sys.exit()

if __name__ == "__main__":
    cmd = main()
