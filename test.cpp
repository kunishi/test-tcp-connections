#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/uio.h>
#include <unistd.h>

using namespace std;

int main()
{
    int s, cc;
    struct sockaddr_in sa;
    char buf[1024];
    char send[] = "getready\n";
    string send_str = string(send);
    int status = 0;

    if ((s = socket(AF_INET, SOCK_STREAM, 0)) == 1) {
        perror("socket");
        exit(1);
    }

    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(12345);
    sa.sin_addr.s_addr = inet_addr("127.0.0.1");

    fprintf(stderr, "Connecting to the server...\n");
    if (connect(s, (struct sockaddr *)&sa, sizeof(sa)) == -1) {
        perror("connect");
        exit(1);
    }
    fprintf(stderr, "Connected.\n");

    fprintf(stderr, "Sending to the server:\n");
    status = write(s, send, strlen(send));
    fprintf(stderr, "Sent.: %d\n", status);

    while ((cc = read(s, buf, sizeof(buf))) > 0) {
        write(1, buf, cc);
    }

    if (close(s) == -1) {
        perror("close");
        exit(1);
    }
}
